var helpers = require("../lib/helpers");

module.exports = {
	dump: function(test) {
		test.equal(helpers.dump({}), "{}");
		test.equal(helpers.dump(1,2), "[ 1, 2 ]");
		test.done();
	}
};
