var _ = require("lodash");

module.exports = {
  loadConfig: function(test) {
    var config = require("../lib/config");
    test.ok(_.isObject(config));
    test.equal(config.db.driver, "pg");
    test.equal(config.db.database, "pmd_dev");
    test.done();
  }
};

