var transport = require("../lib/transport");

module.exports = {
  fetch: function(test) {
    test.expect(1);
    transport.fetch("http://yandex.ru/", function(err, page) {
      if (err) throw err;
      test.ok(~page.indexOf("Яндекс"));
      test.done();
    });
  }
};
