var log = require("../lib/logger").getLogger("test");
var sinon = require("sinon");

module.exports = {
  setUp: function(callback) {
    sinon.spy(process.stdout, "write");
    callback();
  },
  tearDown: function(callback) {
    process.stdout.write.restore();
    callback();
  },
  consoleLogging: function(test) {
    var message = "Doing foo";
    log.trace(message);
    log.debug(message);
    log.info(message);
    log.warn(message);
    log.error(message);
    log.fatal(message);
    test.equal(process.stdout.write.callCount, 6);
    test.ok(process.stdout.write.alwaysCalledWithMatch(message));
    test.done();
  }
};
