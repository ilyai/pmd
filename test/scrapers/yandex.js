var fs = require("fs");
var yandex = require("../../scrapers/yandex");

module.exports = {
  setUp: function(done) {
    this.page = fs.readFileSync(__dirname + "/../fixtures/yandex.html").toString();
    this.target = {
      id: 1,
      url: "http://pogoda.yandex.ru/perm"
    };
    done();
  },
  parse: function(test) {
    var data = yandex.parse(this.page, this.target);
    console.dir(data);
    test.done();
  }
};
