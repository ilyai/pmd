var log = require("../../lib/logger").getLogger("yandex");
var helpers = require("../../lib/helpers");
var cheerio = require("cheerio");

exports.scrape = function(target) {
  transport.request(target.url, (function(err, html) {
    if (err) return;
    // TODO
  }).bind(this));
};

exports.save = function(results) {
  console.dir(results);
};

exports.parse = function(html, target) {
  var $ = cheerio.load(html);
  var results = [];
  var tree = {};

  // Factual
  var factual = {};
  factual.target = target;
  // factual.date = datetime().tz(target.timezone);
  
  tree.container = $(".l-layout_layout_current-weather");
  tree.extra = tree.container.find(".b-thermometer-info__line");
  tree.extra.find(".b-thermometer-info__mark").empty();

  var property;
  tree.extra.each(function(i, el) {
    switch (i) {
      case 0: property = "pressure"; break;
      case 1: property = "wind"; break;
      case 2: property = "humidity"; break;
      default: return;
    }
    if (property === "wind") {
      var wind = $(el).text().split(/,\s*/);
      factual.wind_dir = wind[0];
      factual.wind_speed = wind[1];
    } else {
      factual[property] = $(el).text();
    }
  });

  factual.temp = tree.container.find(".b-thermometer__now").text();
  results.push(factual);

  // Forecast
  tree.container = $(".b-forecast.b-forecast_line_9");
  tree.days        = tree.container.find("tr").eq(0).find(".b-forecast__item");
  tree.phenomenon  = tree.container.find("tr").eq(1).find(".b-forecast__item");
  tree.dailytemp   = tree.container.find("tr").eq(2).find(".b-forecast__item");
  tree.nigthlytemp = tree.container.find("tr").eq(3).find(".b-forecast__item");

  tree.days.each(function(i, el) {
    if ($(el).hasClass("b-forecast__gap")) return;
    // TODO: Night temperature?
    // tree.$nigthlytemp.eq(i).find(".b-forecast__tnight").text()
    
    results.push({
      // date: helpers.dateByDay(
      //           parseInt($(el).find(".b-forecast__day").text(), 10)),
      target: target,
      temp: tree.dailytemp.eq(i).find(".b-forecast__tday").text()
    });
  });

  return results;
};
