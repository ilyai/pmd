var phantomProxy = require('phantom-proxy').create({}, function(proxy){
  var page = proxy.page,
  phantom = proxy.phantom;

  page.open("http://pogoda.yandex.ru/perm/", function(status) {
      page.injectJs("../lib/jquery.js");
      page.evaluate(function() {
          var $current = $('.l-layout_layout_current-weather');
          var record = {};
          record.temperature = $current.find('.b-thermometer__now').text();
          $current.find('.b-thermometer-info__line').each(function(i, el) {
            var property;
            // console.dir(el);
            switch (i) {
              case 0: property = 'pressure'; break;
              case 1: property = 'wind'; break;
              case 2: property = 'humidity'; break;
              default: return;
            }
            record[property] = $(el).contents().eq(1).text();
          });
          $current.css('border', '1px dashed red');
          // console.log(JSON.stringify(record));
          return JSON.stringify(record);
      }, function(result) {
        console.dir(JSON.parse(result));
        page.render('weather.png', function() {
          phantomProxy.end();
        });
      });
  });

});
