var jsdom = require("jsdom");
var fs = require('fs');
var jquery = fs.readFileSync('../lib/jquery.js').toString();

jsdom.env({
  html: "http://pogoda.yandex.ru/perm/",
  src: [jquery],
  done: function (errors, window) {
    var $ = window.$;
    var $current = $('.l-layout_layout_current-weather');
    var record = {};
    record.temperature = $current.find('.b-thermometer__now').text();
    $current.find('.b-thermometer-info__line').each(function(i, el) {
      var property;
      // console.dir(el);
      switch (i) {
        case 0: property = 'pressure'; break;
        case 1: property = 'wind'; break;
        case 2: property = 'humidity'; break;
        default: return;
      }
      record[property] = $(el).contents().eq(1).text();
    });
    console.dir(record);
  }
});
