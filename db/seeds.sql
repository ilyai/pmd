begin transaction;

truncate table locations cascade;

insert into locations (country, city, timezone)
  values ('Россия', 'Пермь', 'Asia/Yekaterinburg');

truncate table providers cascade;

insert into providers (name, url)
  values ('Yandex', 'http://pogoda.yandex.ru/');

truncate table targets cascade;

insert into targets (provider_id, location_id, url)
  values (
    (select provider_id
      from providers
      where name = 'Yandex'),
    (select location_id
      from locations
      where country = 'Россия' and city = 'Пермь'),
    'http://pogoda.yandex.ru/perm'
  );

commit transaction;
