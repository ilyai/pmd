
-- Table: locations

drop table if exists locations cascade;

create table locations
(
  location_id serial primary key,
  country character varying(255) not null,
  city character varying(255) not null,
  timezone character varying(255) not null,
  unique (country, city)
);

-- Table: providers

drop table  if exists providers cascade;

create table providers
(
  provider_id serial primary key,
  name character varying(255) not null,
  url character varying(255),
  unique (name)
);

-- Table: targets

drop table  if exists targets cascade;

create table targets
(
  target_id serial primary key,
  provider_id integer not null
    references providers
    on update cascade
    on delete cascade,
  location_id integer not null
    references locations
    on update cascade
    on delete cascade,
  url character varying(255) not null,
  unique (provider_id, location_id)
);

-- Table: factuals

drop table  if exists factuals cascade;

create table factuals
(
  factual_id bigserial primary key,
  created_at timestamp with time zone not null default now(),
  date timestamp with time zone not null,
  target_id integer not null
    references targets
    on update cascade
    on delete cascade,
  temp integer not null
    check (temp > -100 and temp < 100),
  pressure integer not null
    check (pressure > 500 and pressure < 1000),
  humidity integer not null
    check (humidity >= 0 and humidity <= 100),
  wind_speed double precision,
  wind_dir character varying(3),
  unique (date, target_id)
);

-- Table: forecasts

drop table if exists forecasts cascade;

create table forecasts
(
  forecast_id bigserial primary key,
  created_at timestamp with time zone not null default now(),
  date timestamp with time zone not null,
  target_id integer not null
    references targets
    on update cascade
    on delete cascade,
  temp integer not null
    check (temp > -100 and temp < 100),
  pressure integer,
  humidity integer,
  prcp integer,
  wind_speed double precision,
  wind_dir character varying(3),
  unique (date, target_id)
);
