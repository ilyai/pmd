SRC = lib/*.js scrapers/yandex/*.js test/*.js

lint: $(SRC)
	jshint $(SRC)

deploy:
	git push
	sudo -u www-data sh -c "cd /srv/www/pmd && git pull"
