var _ = require("lodash");

var env = process.env.NODE_ENV || "development";
var config = require("../config.json");
var overrides = require("../config." + env + ".json");

module.exports = _.merge(config, overrides);
