var util = require("util");
var pg = require("pg");
var named = require("node-postgres-named");
var config = require("../../config.json").db;

exports.query = query;
exports.shutdown = shutdown;

console.dir(config);

function query(/* text, [values], callback */) {
	var args = arguments;
	pg.connect(config, function(err, client, done) {
		if (err) throw err;
		if (config.logging && args[0])
			util.log("[pg] => " + args[0].replace(/\n/g, " "));
		named.patch(client);
		client.query.apply(client, args);
		client.on("drain", function() {
			util.log("[pg] client drained");
			done();
		});
	});
}

function shutdown() {
	util.log("[pg] closing connections...");
	pg.end();
}
