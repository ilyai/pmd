var url = require("url");
var util = require("util");
var _ = require("underscore");

function dump(/* arg|arg... */) {
	var x = arguments.length > 1 ? _.toArray(arguments) : arguments[0];
	return util.inspect(x).replace(/\n\s*/g, " ");
}

function makeUrl(host, href) {
  if (url.parse(href).hostname) {
    return href;
  } else {
    return url.format(_.extend(url.parse(host), url.parse(href)));
  }
}

exports.dump = dump;
exports.makeUrl = makeUrl;

/*
// Assume the day within current month
exports.dateByDay = function(day) {
  var date = new Date();
  var today = date.getDay();
  if (day < today) {
    if (date.getMonth() + 1 <= 11) {
      date.setMonth(date.getMonth() + 1);
    } else {
      date.setFullYear(date.getFullYear() + 1);
      date.setMonth(0);
    }
  }
  date.setDate(day);
  return date;
};

// Dump document on exception for further inspection
exports.dumpDocument = function(doc, scraperName, target) {
  var filename = util.format("/tmp/%s_%d_%d.html",
    scraperName.toLowerCase(),
    target.target_id,
    (new Date()).valueOf());
  log.info("Dumping document to " + filename + "...");
  return fs.writeFileSync(filename, doc);
};
*/
