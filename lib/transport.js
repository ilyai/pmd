var requestJS = require("request");
var util = require("util");
var http = require("http");
var log = require("./logger").getLogger("transport");

var headers = {
	"User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0",
	"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
	"Accept-Language": "en-US,en;q=0.5",
	// "Accept-Encoding": "gzip, deflate",
	"Accept-Charset": "UTF-8,*;q=0.5"
};

exports.fetch = function(url, callback) {
  requestJS({url: url, headers: headers}, function(error, response, data) {
    if (error) return log.error(error);

    if (response.statusCode === 200) {
      log.info(util.format("[%d] %s", response.statusCode, url));
    } else {
      log.error(util.format("[%d] %s", response.statusCode, url));
    }

    callback(error, data);
  });
};

exports.request = requestJS;
