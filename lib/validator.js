var util = require("util");
var _ = require("underscore");
var helpers = require("../lib/helpers");

var directions = [
  "N", "NNE", "NE", "ENE",
  "E", "ESE", "SE", "SSE",
  "S", "SSW", "SW", "WSW",
  "W", "WNW", "NW", "NNW",
  null // calm
];

var directionMap = {
  ru: {
    "СЕВЕРНЫЙ": "N",
    "СЕВЕРО-СЕВЕРО-ВОСТОЧНЫЙ": "NNE",
    "СЕВЕРО-ВОСТОЧНЫЙ": "NE",
    "ВОСТОЧНО-СЕВЕРО-ВОСТОЧНЫЙ": "ENE",
    "ВОСТОЧНЫЙ": "E",
    "ВОСТОКО-ЮГО-ВОСТОЧНЫЙ": "ESE",
    "ЮГО-ВОСТОЧНЫЙ": "SE",
    "ЮГО-ЮГО-ВОСТОЧНЫЙ": "SSE",
    "ЮЖНЫЙ": "S",
    "ЮГО-ЮГО-ЗАПАДНЫЙ": "SSW",
    "ЮГО-ЗАПАДНЫЙ": "SW",
    "ЗАПАДНО-ЮГО-ЗАПАДНЫЙ": "WSW",
    "ЗАПАДНЫЙ": "W",
    "ЗАПАДНО-СЕВЕРО-ЗАПАДНЫЙ": "WNW",
    "СЕВЕРО-ЗАПАДНЫЙ": "NW",
    "СЕВЕРО-СЕВЕРО-ЗАПАДНЫЙ": "NNW",
    "ШТИЛЬ": null
  }
};

var validators = {
  exists: function(v) {
    /* jshint eqnull: true */
    if (v != null || v !== "") return "Non exitsten value";
  },
  number: function(v) {
    if (!_.isNumber(v)) return "Not a number";
  },
  finite: function(v) {
    if (!_.isFinite(v)) return "Not a finite number";
  },
  direction: function(v) {
    if (!~directions.indexOf(v)) return "Not a cardinal direction";
  },
  pressure: function(v) {
    if (!_.isNumber(v) || v < 500 || v > 1000) return "Not a pressure";
  },
  wind_speed: function(v) {
    if (!_.isNumber(v) || v < 0.0 || v > 200.0) return "Not a wind speed";
  },
  humidity: function(v) {
    if (!_.isNumber(v) || v < 0 || v > 100.0) return "Not a humidity";
  },
  temperature: function(v) {
    if (!_.isNumber(v) || v < -100 || v > 100) return "Not a temperature";
  }
};

function validate(v) {
  return {
    as: function(type) {
      if (!(type in validators)) throw Error("No such validator: " + type);
      var msg = validators[type](v);
      if (msg) throw Error(msg + ": " + helpers.dump(v));
      return this;
    }
  };
}

exports.validate = validate;

// Sanitizer.prototype.toString = function() {
//   this.value = this.value.toString().trim();
//   this.nonblank();
//   return this.value;
// };
// Sanitizer.prototype.toInteger = function() {
//   this.value = normalizeString(this.value);
//   this.value = parseInt(this.value, 10);
//   this.finite();
//   return this.value;
// };
// Sanitizer.prototype.toFloat = function() {
//   this.value = normalizeString(this.value);
//   this.value = parseFloat(this.value, 10);
//   this.finite();
//   return this.value;
// };
// Sanitizer.prototype.toDirection = function() {
//   this.toString();
//   this.value = directionMap.ru[this.value.toUpperCase()];
//   this.direction();
//   return this.value;
// };
// Sanitizer.prototype.toPressure = function() {
//   this.toInteger();
//   this.pressure();
//   return this.value;
// };
// Sanitizer.prototype.toWindSpeed = function() {
//   this.toFloat();
//   this.windSpeed();
//   return this.value;
// };
// Sanitizer.prototype.toHumidity = function() {
//   this.toInteger();
//   this.humidity();
//   return this.value;
// };
// Sanitizer.prototype.toTemperature = function() {
//   this.toInteger();
//   this.temperature();
//   return this.value;
// };
//
