var util = require("util");
var _ = require("underscore");
var validator = require("../validator");
var db = require("../database");

function validate(factual) {
	validator.validate(factual.date).as("exists");
	validator.validate(factual.temp).as("temperature");
	validator.validate(factual.pressure).as("pressure");
	validator.validate(factual.humidity).as("humidity");
	validator.validate(factual.wind_speed).as("wind_speed");
	validator.validate(factual.wind_dir).as("direction");
      // case "target_id":
      //   val.nonblank();
      //   break;
      // case "date":
      //   val.nonblank();
      //   break;
      // case "temp":
      //   val.temperature();
      //   break;
      // case "pressure":
      //   val.pressure();
      //   break;
      // case "humidity":
      //   val.humidity();
      //   break;
      // case "wind_speed":
      //   val.windSpeed();
      //   break;
      // case "wind_dir":
      //   val.direction();
      //   break;
}

function save(factual, callback) {
  validate(factual);

  return db.query(
		"insert into factuals\n" +
		"  (date, target_id, temp, pressure, humidity, wind_speed, wind_dir)\n" +
		"values\n" +
		"  ($date, $target_id, $temp, $pressure, $humidity, $wind_speed, $wind_dir)\n" +
		"returning *",
		factual, callback);

function findByTarget(id) {
  return db.query(sql.selectByTarget, [id]);
}
