var timezoneJS = require('timezone-js'),
    tz = timezoneJS.timezone,
    fs = require('fs');

tz.loadingScheme = tz.loadingSchemes.MANUAL_LOAD;
tz.loadZoneDataFromObject(JSON.parse(
  fs.readFileSync(__dirname + '/../timezone/tzdata.json').toString()));
module.exports = timezoneJS;
