var fs = require('fs');
var moment = require('moment-timezone');
var tzData = JSON.parse(fs.readFileSync(__dirname + '/../vendor/tzdata.json'));

moment.tz.add(tzData);
module.exports = exports = moment;
